package ru.t1.akolobov.tm.logger.listener;

import lombok.SneakyThrows;
import ru.t1.akolobov.tm.logger.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class EntityListener implements MessageListener {

    private final LoggerService loggerService;

    public EntityListener(final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @SneakyThrows
    public void onMessage(final Message message) {
        if (message instanceof TextMessage) {
            final TextMessage textMessage = (TextMessage) message;
            loggerService.log(textMessage.getText());
        }
    }

}
