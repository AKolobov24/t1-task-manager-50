package ru.t1.akolobov.tm.logger.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;

import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MongoClient mongoClient = new MongoClient("localhost", 27017);

    private final MongoDatabase database = mongoClient.getDatabase("mongo");

    @SneakyThrows
    public void log(String message) {
        final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        final String collectionName = event.get("table").toString();
        if (database.getCollection(collectionName) == null) database.createCollection(collectionName);
        final MongoCollection<Document> collection = database.getCollection(collectionName);
        collection.insertOne(new Document(event));
        System.out.println(message);
    }

}
